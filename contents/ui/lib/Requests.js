function getTeams(callback) {
  const req = new XMLHttpRequest();
  const url = "https://statsapi.web.nhl.com/api/v1/teams";

  req.open("GET", url);
  req.send();

  req.onreadystatechange = (e) => {
    if (req.readyState === XMLHttpRequest.DONE) {
      callback(req.response);
    }
  };
}

function getCurrentSeason(callback) {
  const req = new XMLHttpRequest();
  const url = "https://statsapi.web.nhl.com/api/v1/seasons/current";

  req.open("GET", url);
  req.send();

  req.onreadystatechange = (e) => {
    if (req.readyState === XMLHttpRequest.DONE) {
      callback(req.response);
    }
  };
}

function getSeasonSchedule(teamId, callback) {
  getCurrentSeason((season) => {
    const seasonId = JSON.parse(season).seasons[0].seasonId;
    const req = new XMLHttpRequest();
    let url = "https://statsapi.web.nhl.com/api/v1/schedule?season=" + seasonId;

    if (teamId !== -1) url += "&teamId=" + teamId.toString();

    req.open("GET", url);
    req.send();

    req.onreadystatechange = (e) => {
      if (req.readyState === XMLHttpRequest.DONE) {
        callback(req.response);
      }
    };
  });
}

function getSeasonStandings(callback) {
  const req = new XMLHttpRequest();
  let url = "https://statsapi.web.nhl.com/api/v1/standings";

  req.open("GET", url);
  req.send();

  req.onreadystatechange = (e) => {
    if (req.readyState === XMLHttpRequest.DONE) {
      callback(req.response);
    }
  };
}
