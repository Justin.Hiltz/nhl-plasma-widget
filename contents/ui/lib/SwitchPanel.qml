/*
 * Copyright 2018  Friedrich W. H. Kossebau <kossebau@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9

import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

ColumnLayout {
    id: root

    function getUTCTime(date) { 
        return new Date(
            date.getUTCFullYear(),
            date.getUTCMonth(),
            date.getUTCDate(),
            date.getUTCHours(),
            date.getUTCMinutes(), 
            date.getUTCSeconds()
        ).getTime(); 
    }

    function formatTime(stringDate) {
        const date = new Date(stringDate)
        let hours = date.getHours()
        let minutes = date.getMinutes()
        let meri = "AM"

        if (minutes < 10) {
            minutes = "0" + minutes
        }

        if (hours > 12) {
            meri = "PM"
            hours -= 12
        }

        return hours + ":" + minutes + " " + meri
    }

    function formatDateUTC(stringDate) {
        const date = new Date(stringDate)
        const months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        const weekdays = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]

        const month = months[date.getUTCMonth()]
        const weekday = weekdays[date.getUTCDay()]
        return weekday + " " + month + " " + date.getUTCDate() + ", " + date.getUTCFullYear()
    }

    function todayIndex() {
        let todayIdx = 0
        const today = Date.now()

        for (const [idx, day] of schedule.entries()) {
            const date = new Date(day.date)

            if (getUTCTime(date) + 24 * 60 * 60 * 1000 >= today) {
                todayIdx = idx
                break
            }
        }

        return todayIdx
    }

    function detailsString(game) {
        const time = formatTime(game.gameDate)
        const homeScore = game.teams.home.score
        const awayScore = game.teams.away.score
        const gameStatus = parseInt(game.status.codedGameState)

        if (gameStatus === 1) { // Not started
            return time
        } else if (gameStatus === 7 || gameStatus === 6) { // Final
            return time + " | Final | " + awayScore + " - " + homeScore
        } else if (gameStatus === 9) { // Postponed
            return time + " | Postponed"
        } else { // In progress (or something else??)
            return time + " | " + awayScore + " - " + homeScore
        }
    }

    function divisionAndConference(record) {
        const divName = record.division.name
        const confName = record.conference.name

        if (divName === "") {
            return confName
        }
        if (confName !== "") {
            return divName + " | " + confName
        }
        return divName
    }

    function rankAndName(teamRecord) {
        const name = teamRecord.team.name

        if (teamRecord.conferenceRank === "0" && teamRecord.divisionRank === "0") {
            return "No Rank: " + name
        }
        if (teamRecord.conferenceRank === "0") {
            return teamRecord.divisionRank + ". " + name
        }
        return teamRecord.conferenceRank + ". " + name
    }

    function standingDetails(teamRecord) {
        const played = teamRecord.gamesPlayed
        const leagueRecord = teamRecord.leagueRecord
        const winLossOT = leagueRecord.wins + "-" + leagueRecord.losses + "-" + leagueRecord.ot
        const pts = teamRecord.points
        const ptsPercent = teamRecord.pointsPercentage.toFixed(3)

        return pts + " PTS | " + winLossOT + " | " + played + " GP | " + ptsPercent + " P%"
    }

    property var schedule: []
    property var standings: []
    property var favId: -1

    PlasmaComponents.TabBar {
        id: tabBar

        Layout.fillWidth: true
        visible: true

        PlasmaComponents.TabButton {
            id: scheduleTabButton
            text: "Schedule"
        }

        PlasmaComponents.TabButton {
            id: standingsTabButton
            text: "Standings"
        }
    }


    SwipeView {
        id: swipeView

        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
        Layout.minimumWidth: Math.max(20)
        Layout.minimumHeight: Math.max(40)

        clip: true // previous/next views are prepared outside of view, do not render them

        currentIndex: tabBar.currentIndex

        Component {
            id: gameInfo
            
            Column {
                width: ListView.view.width
                height: 35
                leftPadding: 20

                Text { 
                    width: parent.width 
                    color: "white"
                    text: modelData.teams.away.team.name + " @ " + modelData.teams.home.team.name
                }
                Text {
                    width: parent.width
                    color: "white"
                    text: detailsString(modelData)
                }
                Rectangle {
                    height: 1
                    color: "gray"
                    width: parent.width - 40
                }
            }
        }

        Component {
            id: teamStandingInfo
            
            Column {
                width: ListView.view.width
                height: 35
                leftPadding: 20

                Text { 
                    width: parent.width 
                    color: modelData.team.id === root.favId ? "lightblue" : "white"
                    text: rankAndName(modelData)
                }
                Text {
                    width: parent.width
                    color: "white"
                    text: standingDetails(modelData)
                }
                Rectangle {
                    height: 1
                    color: "gray"
                    width: parent.width - 40
                }
            }
        }

        Component {
            id: listItemSchedule
            
            Column {
                property var day: modelData
                width: ListView.view.width

                Text { 
                    text: formatDateUTC(day.date)
                    color: "white"
                }
                Rectangle {
                    height: 1
                    color: "white"
                    width: 110
                }

                ListView {
                    model: day.games
                    width: parent.width
                    height: count * 35
                    
                    delegate: gameInfo
                }
            }
        }

        Component {
            id: listItemStandings
            
            Column {
                property var record: modelData
                width: ListView.view.width

                Text { 
                    text: divisionAndConference(record)
                    color: "white"
                }
                Rectangle {
                    height: 1
                    color: "white"
                    width: 110
                }

                ListView {
                    model: record.teamRecords
                    width: parent.width
                    height: count * 35
                    
                    delegate: teamStandingInfo
                }
            }
        }

        ListView {
            id: scheduleView
            model: schedule
            property alias schedule: root.schedule
            Layout.fillHeight: true
            Layout.fillWidth: true

            delegate: listItemSchedule
            maximumFlickVelocity: 1500
            spacing: 10
            ScrollBar.vertical: ScrollBar {}
            focus: true
            clip: true

            onCountChanged: {
                positionViewAtIndex(todayIndex(), ListView.Beginning)
            }
        }

        ListView {
            id: standingsView
            model: standings
            property alias schedule: root.standings
            Layout.fillHeight: true
            Layout.fillWidth: true

            delegate: listItemStandings
            maximumFlickVelocity: 1500
            spacing: 10
            ScrollBar.vertical: ScrollBar {}
            focus: true
            clip: true
        }

        onCurrentIndexChanged: {
            tabBar.setCurrentIndex(currentIndex);
        }
    }
}
