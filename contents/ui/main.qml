/*
 *   Copyright 2021 Justin Hiltz <justin.j.hiltz@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.calendar 2.0 as PlasmaCalendar
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import "lib/Requests.js" as Requests
import "lib"

Item {
    id: nhl

    width: units.gridUnit * 20
    height: units.gridUnit * 20
    property string favouriteTeam: plasmoid.configuration.favouriteTeam
    property var schedule: []
    property var standings: []

    Plasmoid.backgroundHints: PlasmaCore.Types.DefaultBackground;

    function scheduleCallback(results) {
        const jsonResults = JSON.parse(results)
        const resultSchedule = []

        jsonResults.dates.forEach((date) => {
            resultSchedule.push(date)
        })

        schedule = resultSchedule
    }

    function standingsCallback(results) {
        const jsonResults = JSON.parse(results)
        standings = jsonResults.records
    }

    Connections {
        target: plasmoid.configuration
        onFavouriteTeamChanged: Requests.getSeasonSchedule(JSON.parse(favouriteTeam).id, scheduleCallback)
    }

    Component.onCompleted: {
        Requests.getSeasonSchedule(JSON.parse(favouriteTeam).id, scheduleCallback)
        Requests.getSeasonStandings(standingsCallback)
    }

    ColumnLayout {
        anchors.fill: parent

        PlasmaExtras.Heading {
            Layout.fillWidth: true
            level: 3
            text: JSON.parse(favouriteTeam).name
        }
        SwitchPanel {
            Layout.fillWidth: true

            schedule: nhl.schedule
            standings: nhl.standings
            favId: JSON.parse(favouriteTeam).id
        }
    }
    
    Timer {
        interval: 1000 * 60;
        running: true;
        repeat: true

        onTriggered: Requests.getSeasonSchedule(JSON.parse(favouriteTeam).id, scheduleCallback)
    }

    Timer {
        interval: 1000 * 60;
        running: true;
        repeat: true

        onTriggered: Requests.getSeasonStandings(standingsCallback)
    }
}
