/*
 *  Copyright 2013 David Edmundson <davidedmundson@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.5 as Kirigami

import "lib"
import "lib/Requests.js" as Requests

ConfigPage {
    id: page
	property var teamList: []
    property bool favouriteTeamPopulated: false

    function getNamesList(results) {
        var jsonResults = JSON.parse(results)
        var teamListResults = []
        jsonResults.teams.forEach((team) => {
            teamListResults.push({ value: JSON.stringify(team), text: i18n(team.name) })
        })
        teamListResults.push({ value: JSON.stringify({ name: "All Teams", id: -1 }), text: "None" })
        teamList = teamListResults
        favouriteTeamCombo.populate()
        favouriteTeamPopulated = true
    }

    Component.onCompleted: {
        Requests.getTeams(getNamesList)
    }

    ColumnLayout {
        Kirigami.FormLayout {
            RowLayout {
                Kirigami.FormData.label: i18n("Favourite Team:")
                ConfigComboBox {
                    id: "favouriteTeamCombo"
                    configKey: "favouriteTeam"
                    populated: favouriteTeamPopulated
                    onPopulate: {
                        model = teamList
                        selectValue(configValue)
                    }
                }
            }
        }
    }
}
