<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">NHL Plasma Widget</h3>

  <p align="center">
    Schedule and Stats for NHL Teams
    <br />
    <a href="https://gitlab.com/Justin.Hiltz/nhl-plasma-widget"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/Justin.Hiltz/nhl-plasma-widget/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Justin.Hiltz/nhl-plasma-widget/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <oldownload/>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

This is a KDE plasma widget to show some NHL data available throught the public API. There doesn't seem to be any existing hockey or NHL (or any sport for that matter) widget on the KDE store so I thought I would try making one.

<!-- GETTING STARTED -->

## Getting Started

The simplest way to download/install this widget will be from the [KDE Store](https://store.kde.org/) once development is finished.

Until then, or if you'd prefer to install it manually, use the following.

### Prerequisites

- The Plasma desktop environment
- Development Only: If you plan to work on development of this widget the `plasma-sdk` package offers useful tools such as `plasmoidviewer`.

### Installation

1. Make and change to the directory `~/.local/share/plasma/plasmoids/`
   ```sh
   mkdir ~/.local/share/plasma/plasmoids/ && cd ~/.local/share/plasma/plasmoids/
   ```
2. Clone the repo
   ```sh
   git clone https://gitlab.com/Justin.Hiltz/nhl-plasma-widget
   ```
3. Change the project folder name to `com.gitlab.Justin.Hiltz.nhl`
   ```sh
   mv nhl-plasma-widget com.gitlab.Justin.Hiltz.nhl
   ```
4. Either:
   - Add the widget normally (i.e. Right-click the Desktop -> Add Widgets... -> Select NHL widget) <b>OR</b>
   - Test the widget
     ```sh
     plasmoidviewer -a com.github.zren.nhl
     ```

<!-- USAGE EXAMPLES -->

## Usage

The widget should immediately show this seasons schedule and standings.

You can configure your favourite team using the widget's configuration and this will limit the schedule to games involving your team and highlight them in the standings.

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://gitlab.com/Justin.Hiltz/nhl-plasma-widget/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->

## License

Distributed under the GNU Public License. See `LICENSE` for more information.

<!-- CONTACT -->

## Contact

Justin Hiltz - [@justinjhiltz](https://twitter.com/justinjhiltz) - justin.j.hiltz@hotmail.com

Project Link: [https://gitlab.com/Justin.Hiltz/nhl-plasma-widget](https://gitlab.com/Justin.Hiltz/nhl-plasma-widget/)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [Readme Template](https://github.com/othneildrew/Best-README-Template)
- [Widget Example](https://zren.github.io/kde/docs/widget/)
- [Widget Tutorial](https://develop.kde.org/docs/plasma/widget/)
- [QML Docs](https://doc.qt.io/qt-5/qtqml-documents-topic.html)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[gitlab-url]: https://gitlab.com/Justin.Hiltz/nhl-plasma-widget
